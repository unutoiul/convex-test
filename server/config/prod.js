const config = {
	env: "dev",
	port: 8000,
};

global.config = config;

module.exports = config;
