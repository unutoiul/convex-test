import env from "node-env-file";
import dotenv from "dotenv";

dotenv.config();

var config = require("./" + process.env.NODE_ENV);

global.config = config;

module.exports = config;
