import db from 'services/db';

const Route = {
    method: 'GET',
    path: '/api/submission/{submissionReference?}',
    config: {
        auth: false
    },
    handler: async (request, reply) => {
        const submissionReference = (request.params.submissionReference) ? request.params.submissionReference : null;

        if (submissionReference) {
            const results = db.findById(submissionReference);

            if (!results) {
                return reply({ message: 'No submission found!' });
            }

            const totalRisks = results.risks.length;
            const totalPrice = (results.risks.length >= 2) ? results.risks.reduce((prev, curr) => prev.price + curr.price) : results.risks[0].price;
            const averagePrice = totalPrice / totalRisks;

            const data = {
                averagePrice,
                totalPrice,
                totalRisks
            }

            return reply({ data });
        }

        return reply({ data: db.data });
    }
};

export default Route;