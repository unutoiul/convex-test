import Boom from 'boom';
import Joi from '@hapi/joi';
import db from 'services/db';

const schema = Joi.object({
    submissionReference: Joi.string().required(),
    insured: Joi.string(),
    riskReference: Joi.string(),
    classOfBusiness: Joi.string(),
    price: Joi.number(),
    risks: Joi.array().items(
        Joi.object({
            riskReference: Joi.string().required(),
            classOfBusiness: Joi.string(),
            price: Joi.number(),
        })).unique('riskReference')
});

const Route = {
    method: 'POST',
    path: '/api/submission',
    config: {
        auth: false
    },
    handler: async (request, reply) => {
        const payload = request.payload ? request.payload : null;
        const { error, value } = schema.validate(payload);

        const mutipleSubmissions = (payload.risks) ? true : false;

        if (error) {
            return reply(Boom.badRequest(error.details[0].message));
        }

        //check if submission reference is unique
        const duplicated = db.findById(payload.submissionReference);

        if (duplicated) {
            return reply(Boom.badRequest('The submission reference is duplicated in the database'));
        }

        //will add only one submission
        if (!mutipleSubmissions) {
            db.add({
                submissionReference: payload.submissionReference,
                insured: payload.insured,
                risks: [
                    {
                        riskReference: payload.riskReference,
                        classOfBusiness: payload.classOfBusiness,
                        price: payload.price
                    }
                ]
            })
        } else {
            //add to memory db
            db.add(payload);
        }

        return reply({ data: value });
    }
};

export default Route;