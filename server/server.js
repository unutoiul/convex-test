import Hapi from "hapi";
import Path from "path";

import fs from "fs";
import vision from "vision";
import inert from "inert";

import config from "config";

let server = new Hapi.Server();


server.connection({
    port: config.port,
    routes: {
        cors: true
    },
    labels: "api"
});

//Register Plugins
server.register([vision, inert], { select: ["api"] }, err => {
    if (err) {
        return err;
    }
});

// Load routes directory recursively
let requireDir = dir => {
    for (let p of fs.readdirSync(dir)) {
        let fpath = Path.resolve(dir, p);
        let stat = fs.lstatSync(fpath);
        if (stat.isDirectory()) {
            requireDir(fpath);
        } else if (stat.isFile()) {
            let route = require(fpath);
            server.route(route.default);
        }
    }
};

requireDir("routes");

export default server;
