const db = {
    data: []
};

db.add = (data) => {
    db.data.push(data);
}

db.findById = (submissionReference) => {
    const results = db.data.find(o => (o.submissionReference === submissionReference));
    return results;
}

db.getAll = () => {
    return db.data;
}


export default db;