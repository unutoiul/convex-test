"use strict";

var babel = require("babel-register");
var polyfill = require("babel-polyfill");
var server = require("./server.js");

// Start the server
server.default.start(err => {
    server.default.connections.forEach(function(connection) {
        console.log("Server started at: " + connection.info.uri);
    });
});
