# Convex

## Instalation

```
npm install || yarn

npm start || yarn start

```

## API
### GET BY ID

GET
http://localhost:8000/api/submission/{submissionReference}

### GET ALL SUBMISSIONS

GET
http://localhost:8000/api/submission

### ADD A SUBMISSION

POST
http://localhost:8000/api/submission

BODY:
```
{
    "data": {
        "submissionReference": "A000001",
        "insured": "Knight Frank",
        "risks": [
            {
                "riskReference": "R000001",
                "classOfBusiness": "Property",
                "price": 100000
            },
            {
                "riskReference": "R000002",
                "classOfBusiness": "Property",
                "price": 100000
            }
        ]
    }
}

```